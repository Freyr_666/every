package com.example.every;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Registrar extends Activity {
	Button login;
	EditText TxtUsuario;
	EditText TxtPassword;
	TextView LblMensaje;
	ConnectionDetector con;
	String ufinal;
	String idfinal;
	Httppostaux post;
	String IP_Server = "www.meanmatxine.com";// IP DE NUESTRO PC
	String URL_connect = "http://" + IP_Server + "/primark/acces.php";// ruta
	ProgressDialog pDialog;

	// json
	private static final String USER = "usuarios";
	private static final String userID = "userID";
	private static final String userName = "userName";
	JSONObject json;
	JSONArray usuarios;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registro);

		login = (Button) findViewById(R.id.BtnAceptar);
		TxtUsuario = (EditText) findViewById(R.id.TxtUsuario);
		TxtPassword = (EditText) findViewById(R.id.TxtPassword);
		LblMensaje = (TextView) findViewById(R.id.LblMensaje);
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String usuario = TxtUsuario.getText().toString();
				String passw = TxtPassword.getText().toString();
				ufinal = TxtUsuario.getText().toString();

				// verificamos si estan en blanco
				if (checklogindata(usuario, passw) == true) {

					// verificanos si hay conexion
					con = new ConnectionDetector(Registrar.this);
					if (con.isConnectingToInternet()) {
						new asynclogin().execute(usuario, passw);
						// LblMensaje.setText("correcto");
					} else {
						LblMensaje
								.setText("Desconectado, no podra logearse ahora");
					}

				} else {
					// si detecto un error en la primera validacion vibrar y
					// mostrar un Toast con un mensaje de error.
					LblMensaje.setText("Debe rellenar ambos campos");
				}

			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inicial, menu);

		return true;

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.desconectar:
			// borrar GCM
			final String regId = GCMRegistrar.getRegistrationId(getApplicationContext());
			if (!regId.equals("")) {
				GCMRegistrar.unregister(getApplicationContext());
			} else {
				Log.v("GCMTest", "Ya des-registrado");
			}
			// borrar Usuario
			DBCUsuario user = new DBCUsuario(this, "DBCUsuario", null, 1);
			SQLiteDatabase db = user.getWritableDatabase();
			db.execSQL("DELETE FROM Usuarios");
			db.close();
			// cerrar aplicacion
			System.exit(0);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	/* ############### checklogindata ###################### */
	// validamos si no hay ningun campo en blanco
	public boolean checklogindata(String user, String pass) {
		if (user.equals("") || pass.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	/* ############### asynclogin #################### */
	class asynclogin extends AsyncTask<String, String, String> {
		String user, pass;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Registrar.this);
			pDialog.setMessage("Autenticando....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... params) {
			user = params[0];
			pass = params[1];
			InputStream is = null;
			String json2 = "";
			JSONParser jParser = new JSONParser();

			Log.d("user", user);
			Log.d("pass", pass);
			ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

			postparameters2send.add(new BasicNameValuePair("usuario", user));
			postparameters2send.add(new BasicNameValuePair("password", pass));
			/*
			 * if (loginstatus(user, pass) == true) { return "ok"; // login
			 * valido } else { return "err"; // login invalido }
			 */
			try {
				// defaultHttpClient
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(URL_connect);

				httpPost.setEntity(new UrlEncodedFormEntity(postparameters2send));

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				json2 = sb.toString();
			} catch (Exception e) {
				Log.e("Buffer Error", "Error converting result " + e.toString());
			}

			// try parse the string to a JSON object
			try {
				json = new JSONObject(json2);
			} catch (JSONException e) {
				Log.e("JSON Parser", "Error parsing data " + e.toString());
			}
			try {
				usuarios = json.getJSONArray(USER);
				JSONObject c = usuarios.getJSONObject(0);
				idfinal = c.getString(userID);
				ufinal = c.getString(userName);
				if (idfinal.equals("0")) {
					return "err";
				} else {
					return "ok";
				}
			} catch (JSONException e) {
				return "err";
			}
		}

		protected void onPostExecute(String result) {
			pDialog.dismiss();
			Log.d("onPostExecute=", "" + result);
			if (result.equals("err")) {
				LblMensaje.setText("Usuario o contrase�a invalidos");
			} else {
				// grabar usuario
				DBCUsuario user = new DBCUsuario(Registrar.this, "DBCUsuario",
						null, 1);
				SQLiteDatabase db = user.getWritableDatabase();
				//borramos tabla por si acaso
				db.execSQL("DELETE FROM Usuarios");
				//a�adimos usuario
				String sql = "INSERT INTO Usuarios (id, nombre) VALUES ('"+idfinal+"', '" + ufinal
						+ "');";
				//Log.d("sql", sql);
				db.execSQL(sql);
				db.close();
				//registrar GCM
				// Comprobamos si est� todo en orden para utilizar GCM
				//GCMRegistrar.checkDevice(getApplicationContext());
				GCMRegistrar.checkDevice(getApplicationContext());
				GCMRegistrar.checkManifest(getApplicationContext());
				final String regId = GCMRegistrar
						.getRegistrationId(getApplicationContext());
				if (regId.equals("")) {
					GCMRegistrar.register(getApplicationContext(), "678293160095"); // Sender																			// ID
				} else {
					Log.v("GCMTest", "Ya registrado");
				}
				// arrancar actividad
				Intent intent = new Intent(Registrar.this, Inicial.class);
				startActivity(intent);
			}
		}
	}

	/* #### loginstatus #### */
	public boolean loginstatus(String user, String pass) {
		int logstatus = -1;
		post = new Httppostaux();
		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("usuario", user));
		postparameters2send.add(new BasicNameValuePair("password", pass));

		JSONArray jdata = post.getserverdata(postparameters2send, URL_connect);
		if (jdata != null && jdata.length() > 0) {
			JSONObject json_data;
			try {
				json_data = jdata.getJSONObject(0);
				logstatus = json_data.getInt("logstatus");// accedemos al valor
				Log.e("loginstatus", "logstatus= " + logstatus);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// validamos el valor obtenido
			if (logstatus == 0) {// [{"logstatus":"0"}]
				Log.e("loginstatus ", "invalido");
				return false;
			} else {// [{"logstatus":"1"}]
				Log.e("loginstatus ", "valido");
				return true;
			}
		} else {
			return false;
		}

	}
	
}