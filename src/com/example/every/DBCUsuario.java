package com.example.every;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBCUsuario extends SQLiteOpenHelper {

	// Sentencia SQL para crear la tabla de Usuarios
	String sqlCreate = "CREATE TABLE Usuarios (id INT, nombre TEXT)";

	public DBCUsuario(Context contexto, String nombre, CursorFactory factory,
			int version) {
		super(contexto, nombre, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Se ejecuta la sentencia SQL de creaci�n de la tabla
		db.execSQL(sqlCreate);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Se elimina la versi�n anterior de la tabla
		db.execSQL("DROP TABLE IF EXISTS Usuarios");
		// Se crea la nueva versi�n de la tabla
		db.execSQL(sqlCreate);

	}
}
