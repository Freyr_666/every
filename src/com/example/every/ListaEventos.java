package com.example.every;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListaEventos extends Activity {
	/*json*/
	private static final String EVENT = "eventos";
	private static final String eventoID = "id";
	private static final String eventoTitulo = "titulo";
	private static final String eventoLugar = "lugar";
	private static final String eventoFecha = "fecha";
	private static final String eventoStamp ="stamp";
	private static final String eventoTipo = "tipo";
	private static final String ltd="ltd";
	private static final String lon="lon";
	/*vars*/
	ProgressDialog pDialog;
	ConnectionDetector con;
	String url="http://www.meanmatxine.com/primark/ver_eventos.php";
	boolean conectado=true;
	JSONArray eventos = null;
	ListView lstOpciones;
	List<Evento> arrayOfEventos;
	JSONObject json;
	EventosAdapter objAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lista_eventos);
		lstOpciones=(ListView)findViewById(R.id.LstOpciones);
		/*#### boton mostrar envento ####*/
		lstOpciones.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				Bundle b = new Bundle();
				String opcion = ((Evento) a.getAdapter().getItem(position))
						.GetID();
				b.putString("eventoID", opcion.toString());

				Intent intent = new Intent(ListaEventos.this,
						EventoUno.class);
				intent.putExtras(b);
				startActivity(intent);

			}
		});
		//comprobar conexion
		con=new ConnectionDetector(ListaEventos.this);
		arrayOfEventos = new ArrayList<Evento>();
		if (con.isConnectingToInternet()) {
			//cargar eventos nuevos
			new asyncwebeventos().execute(url);
		}else{
			//cargar eventos DBC
			new asyncdbceventos().execute();
			//toast error
						
		}			
		//mostrar datos
		for (int i=0;i<arrayOfEventos.size();i++){
			Evento temp=new Evento();
			temp=arrayOfEventos.get(i);
			Log.d("titulo",temp.GetTitulo());
		}
		
	}
	/* #### async dbc ###*/
	
	class asyncdbceventos extends AsyncTask<String, Void, String>{
		
		protected void onPreExecute(){
			// para el progress dialog
						pDialog = new ProgressDialog(ListaEventos.this);
						pDialog.setMessage("Cargando Eventos...");
						pDialog.setIndeterminate(false);
						pDialog.setCancelable(false);
						pDialog.show();
		}
		@Override
		protected String doInBackground(String... params) {
			

			
			//Abrir database
			DBCEvento eventDBC = new DBCEvento(ListaEventos.this, "DBCEventos", null, 1);
			SQLiteDatabase db =eventDBC.getWritableDatabase();

			if (db!=null){
				Cursor c =db.rawQuery("SELECT * FROM Eventos", null);
				if (c.moveToFirst()){
					do{
						Evento temp= new Evento();
						temp.SetID(c.getString(0));
						temp.SetTitulo(c.getString(1));
						temp.SetLugar(c.getString(2));
						temp.SetFecha(c.getString(3));
						temp.SetStamp(c.getString(4));
						temp.SetTipo(c.getString(5));
						temp.Setltd(c.getString(6));
						temp.Setlon(c.getString(7));
						arrayOfEventos.add(temp);
					}while (c.moveToNext());
					
				}
				c.close();
				db.close();
				return "ok";
			}else{
				return "err";
			}
			//return null;
			
		}
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.equals("err")) {
				//mensaje de error
			} else {
				setAdapterToListview();
				Toast toast=Toast.makeText(getApplicationContext(), "No se ha podido leer los eventos, se cargaran los locales",Toast.LENGTH_SHORT);
				toast.show();	
			}
		}	
	}
	
	/*#### async web #### */
	class asyncwebeventos extends AsyncTask<String, Void, String>{
		

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(ListaEventos.this);
			pDialog.setMessage("Cargando Eventos");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... url) {
			//coger eventos de web
			JSONParser jParser = new JSONParser();
			Log.d("url",url[0]);
			json = jParser.getJSONFromUrl(url[0]);
			try {
				eventos = json.getJSONArray(EVENT);
				// looping through All Contacts
				for (int i = 0; i < eventos.length(); i++) {

					JSONObject c = eventos.getJSONObject(i);

					Evento temp = new Evento();
					temp.SetID(c.getString(eventoID));
					temp.SetTitulo(c.getString(eventoTitulo));
					temp.SetFecha(c.getString(eventoFecha));
					temp.SetStamp(c.getString(eventoStamp));
					temp.SetLugar(c.getString(eventoLugar));
					temp.SetTipo(c.getString(eventoTipo));
					temp.Setlon(c.getString(lon));
					temp.Setltd(c.getString(ltd));

					arrayOfEventos.add(temp);			
				}
				return "si";
			} catch (JSONException e) {
				return "err";
			}	
			//return "si";
		}
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.equals("err")) {
				conectado=false;
			} else {
				//cargar eventos
				//Abrir database
				DBCEvento eventDBC = new DBCEvento(ListaEventos.this, "DBCEventos", null, 1);
				SQLiteDatabase db =eventDBC.getWritableDatabase();
				if (db!=null){
					db.execSQL("DELETE FROM Eventos;");
					for (int i=0;i<arrayOfEventos.size();i++){
						Evento temp= new Evento();
						temp=arrayOfEventos.get(i);
						//Log.d("titulo", temp.GetTitulo());
						String sql="INSERT INTO Eventos (eventoID,eventoTitulo,eventoLugar,eventoFecha,eventoStamp,eventoTipo,ltd,lon) VALUES "+
						"('"+temp.GetID()+"','"+temp.GetTitulo()+"','"+temp.GetLugar()+"','"+temp.GetFecha()+"','"+temp.GetStamp()+"','"+temp.GetTipo()+"','"+temp.Getltd()+"','"+temp.Getlon()+"');";
						//Log.d("sql",sql);
						db.execSQL(sql);
					}
					
					db.close();
				}
				setAdapterToListview();
			}
		}	
	}
	/* #### Adaptador #### */
	private void setAdapterToListview() {
		objAdapter = new EventosAdapter(ListaEventos.this,
				R.layout.eventos_adapter, arrayOfEventos);
		lstOpciones.setAdapter(objAdapter);

	}
}
