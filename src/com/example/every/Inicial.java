package com.example.every;

import com.google.android.gcm.GCMRegistrar;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
//import android.widget.TextView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Inicial extends Activity {
	// ConnectionDetector con;
	Boolean registrado;
	String userName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inicial);
		/* ###### CHECK USER ##### */
		DBCUsuario user = new DBCUsuario(this, "DBCUsuario", null, 1);
		SQLiteDatabase db = user.getWritableDatabase();
		
		TextView consulta =(TextView)findViewById(R.id.Consulta);
		

		if (db != null) {
			Cursor c = db.rawQuery("SELECT * FROM Usuarios", null);
			if (c.moveToFirst()) {
				userName = c.getString(1);
				registrado = true;
			} else {
				registrado = false;
			}
			c.close();
		}
		db.close();
		/*a�adir dbco*/
		/*DBCResult result=new DBCResult (this,"DBCResult",null,1);
		SQLiteDatabase dbr = result.getWritableDatabase();
		String sql="";
		if (dbr!= null){
			Cursor cr =dbr.rawQuery("SELECT * FROM Sql", null);
			if (cr.moveToFirst()){
				sql=cr.getString(0);
			}
			cr.close();
		}
		dbr.close();
		consulta.setText(sql);*/
		if (registrado) {
			setTitle("Bienvenido/a " + userName);
			Typeface font = Typeface.createFromAsset(getAssets(),
					"Helvetica_Neue.ttf");
			Button boton1 = (Button) findViewById(R.id.BtnBoton1);
			boton1.setTypeface(font);
			Button boton2 = (Button) findViewById(R.id.BtnBoton2);
			boton2.setTypeface(font);
			Button boton3 = (Button) findViewById(R.id.BtnBoton3);
			boton3.setTypeface(font);

			boton1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// Creamos el Intent
					Intent intent = new Intent(Inicial.this, ListaEventos.class);
					// Iniciamos la nueva actividad
					startActivity(intent);

				}
			});

			// PRUEBAS
			boton3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Inicial.this, Pruebas.class);
					// Iniciamos la nueva actividad
					startActivity(intent);

				}
			});
		} else {
			Intent intent = new Intent(Inicial.this, Registrar.class);
			startActivity(intent);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inicial, menu);

		return true;

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.desconectar:
			// borrar GCM
			final String regId = GCMRegistrar.getRegistrationId(getApplicationContext());
			if (!regId.equals("")) {
				GCMRegistrar.unregister(getApplicationContext());
			} else {
				Log.v("GCMTest", "Ya des-registrado");
			}
			// borrar Usuario
			DBCUsuario user = new DBCUsuario(this, "DBCUsuario", null, 1);
			SQLiteDatabase db = user.getWritableDatabase();
			db.execSQL("DELETE FROM Usuarios");
			db.close();
			// cerrar aplicacion
			System.exit(0);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

}
/*
 * TextView online= (TextView)findViewById(R.id.online); con = new
 * ConnectionDetector(Inicial.this); if (con.isConnectingToInternet()){
 * online.setText("Conectado"); }else{ online.setText("Desconectado"); }
 */