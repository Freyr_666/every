package com.example.every;

public class Evento {
	private String eventoID;
	private String eventoTitulo;
	private String eventoLugar;
	private String eventoFecha;
	private String eventoStamp;
	private String eventoTipo;
	private String ltd;
	private String lon;
	
	/* #### GET #### */
	public String GetID(){
		return eventoID;
	}
	public String GetTitulo(){
		return eventoTitulo;
	}
	public String GetFecha(){
		return eventoFecha;
	}
	public String GetStamp(){
		return eventoStamp;
	}
	public String GetLugar(){
		return eventoLugar;
	}
	public String GetTipo(){
		return eventoTipo;
	}
	public String Getltd(){
		return ltd;
	}
	public String Getlon(){
		return lon;
	}
	/* #### SET #### */
	public void SetID(String idd){
		eventoID=idd;
	}
	public void SetTitulo(String idd){
		eventoTitulo=idd;
	}
	public void SetFecha(String idd){
		eventoFecha=idd;
	}
	public void SetStamp(String idd){
		eventoStamp=idd;
	}
	public void SetLugar(String idd){
		eventoLugar=idd;
	}
	public void SetTipo(String idd){
		eventoTipo=idd;
	}
	public void Setltd(String idd){
		ltd=idd;
	}
	public void Setlon(String idd){
		lon=idd;
	}
}
