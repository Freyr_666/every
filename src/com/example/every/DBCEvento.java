package com.example.every;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBCEvento extends SQLiteOpenHelper{

	String sqlCreate = "CREATE TABLE Eventos (" +
			"eventoID INTEGER  NOT NULL, " +
			"eventoTitulo TEXT  NULL, " +
			"eventoLugar TEXT  NULL, " +
			"eventoFecha TEXT  NULL, " +
			"eventoStamp INTEGER NULL, "+
			"eventoTipo TEXT  NULL, " +
			"ltd DOUBLE  NULL, " +
			"lon DOUBLE  NULL);";
	
	public DBCEvento(Context context, String nombre, CursorFactory factory,
			int version) {
		super(context, nombre, factory, version);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(sqlCreate);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS Eventos");
		// Se crea la nueva versi�n de la tabla
		db.execSQL(sqlCreate);
		
	}

}
