package com.example.every;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class Httppostaux {
	InputStream is = null;
	String result = "";
	HttpResponse r;
	
	public JSONArray getserverdata(ArrayList<NameValuePair> parameters,
			String urlwebserver) {
		
		httppostconnect(parameters, urlwebserver);
		
		if (is != null) {// si obtuvo una respuesta
			Log.d("conexion", "correcta");
			getpostresponse();

			return getjsonarray();

		} else {
			Log.d("conexion", "incorrecta");
			return null;

		}
		//return null;
	}
	/* #### conexion #### */
	public void httppostconnect (ArrayList<NameValuePair> parameters,
			String urlwebserver){
		
		Log.d("url",urlwebserver);
		// conecta via http y envia un post.
		HttpClient client =new DefaultHttpClient();
		HttpPost httppost =new HttpPost(urlwebserver);
		try {
			httppost.setEntity(new UrlEncodedFormEntity(parameters));
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			 r =client.execute(httppost);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity entity = r.getEntity();
		try {
			is = entity.getContent();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/* #### respuesta #### */
	public void getpostresponse() {

		// Convierte respuesta a String
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();

			result = sb.toString();
			Log.e("getpostresponse", " result= " + sb.toString());
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
	}
	/* #### JSON ARRAY ####*/
	public JSONArray getjsonarray() {
		// parse json data
		try {
			JSONArray jArray = new JSONArray(result);

			return jArray;
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
			return null;
		}

	}
}