package com.example.every;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class EventoUno extends Activity {
	private ImageView ImgFoto;
	private TextView LblTitulo;
	private TextView LblFecha;
	private TextView LblLugar;
	private Button BtnMapa;
	Evento temp=new Evento();
	Boolean error=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.evento);

		// variables
		ImgFoto = (ImageView) findViewById(R.id.ImgFoto);
		LblTitulo = (TextView) findViewById(R.id.LblTitulo);
		LblFecha = (TextView) findViewById(R.id.LblFecha);
		LblLugar = (TextView) findViewById(R.id.LblLugar);
		BtnMapa = (Button) findViewById(R.id.BtnMapa);
		// Recuperamos la información pasada en el intent
		Bundle bundle = this.getIntent().getExtras();
		String id = bundle.getString("eventoID");
		//abrir DBC
		DBCEvento eventDBC = new DBCEvento(this, "DBCEventos", null, 1);
		SQLiteDatabase db =eventDBC.getWritableDatabase();
		if (db!=null){
			String sql="SELECT * FROM eventos WHERE eventoID='"+id+"';";
			Cursor c =db.rawQuery(sql, null);
			if (c.moveToFirst()){
				
				temp.SetID(c.getString(0));
				temp.SetTitulo(c.getString(1));
				temp.SetLugar(c.getString(2));
				temp.SetFecha(c.getString(3));
				temp.SetStamp(c.getString(4));
				temp.SetTipo(c.getString(5));
				temp.Setltd(c.getString(6));
				temp.Setlon(c.getString(7));
				c.close();
				db.close();
			}else{
				error=true;
			}
		}else{
			error=true;
		}
		if (!error){
			// setear
			LblTitulo.setText(temp.GetTitulo());
			LblFecha.setText(temp.GetFecha());
			LblLugar.setText(temp.GetLugar());
			Log.d("ltd",temp.Getltd());
			ImgFoto.setImageResource(R.drawable.porsche);
			setTitle(temp.GetTitulo());
			BtnMapa.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Bundle b = new Bundle();
					b.putString("eventoID", temp.GetID().toString());
					Intent intent = new Intent(EventoUno.this,
							MapaEventoUno.class);
					intent.putExtras(b);
					startActivity(intent);
					
				}
			});
		}else{
			LblTitulo.setText("ERROR");
		}
		
		
	
	}
}
