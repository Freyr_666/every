package com.example.every;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Pruebas extends Activity {
	TextView texto;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pruebas);
		
		Bundle bundle = this.getIntent().getExtras();
		String id = bundle.getString("mensaje");
		texto=(TextView)findViewById(R.id.texto);
		texto.setText(id);
		
		
	}
}
