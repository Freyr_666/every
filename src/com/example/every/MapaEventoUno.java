package com.example.every;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MapaEventoUno extends FragmentActivity {
	Evento temp = new Evento();
	Boolean error = false;
	GoogleMap mapa; 

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mapaunevento);
		
		mapa= ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		
		// Recuperamos la información pasada en el intent
		Bundle bundle = this.getIntent().getExtras();
		String id = bundle.getString("eventoID");
		// abrir DBC
		DBCEvento eventDBC = new DBCEvento(this, "DBCEventos", null, 1);
		SQLiteDatabase db = eventDBC.getWritableDatabase();
		if (db != null) {
			String sql = "SELECT * FROM eventos WHERE eventoID='" + id + "';";
			Cursor c = db.rawQuery(sql, null);
			if (c.moveToFirst()) {

				temp.SetID(c.getString(0));
				temp.SetTitulo(c.getString(1));
				temp.SetLugar(c.getString(2));
				temp.SetFecha(c.getString(3));
				temp.SetStamp(c.getString(4));
				temp.SetTipo(c.getString(5));
				temp.Setltd(c.getString(6));
				temp.Setlon(c.getString(7));
				c.close();
				db.close();
			} else {
				error = true;
			}
		} else {
			error = true;
		}
		if (!error){
			setTitle(temp.GetTitulo());
			double lon = Double.parseDouble(temp.Getltd());
			double lat = Double.parseDouble(temp.Getlon());
			LatLng pos= new LatLng(lat,lon);
			String titulo= "lat: "+temp.Getltd()+",lon: "+temp.Getlon();
			CameraUpdate cam=CameraUpdateFactory.newLatLng(pos);
			mapa.moveCamera(cam);
			mostarMarcador (lat, lon, titulo);
			
		}
	}
	private void mostarMarcador (double lat, double lng, String title){
		mapa.addMarker(new MarkerOptions()
		.position(new LatLng(lat,lng))
		.title(title));
	}
}
