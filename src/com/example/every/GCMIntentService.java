package com.example.every;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	String url = "http://www.meanmatxine.com/primark/setGCM.php";

	public GCMIntentService() {
		super("678293160095");
	}

	@Override
	protected void onError(Context contex, String errorID) {
		Log.d("GCMTest", "REGISTRATION: Error -> " + errorID);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		String msg = intent.getStringExtra("message");
		Log.d("GCMTest", "Mensaje: " + msg);
		mostrarNotificacion(context, msg);
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		String id = "";
		// coger nombre
		DBCUsuario user = new DBCUsuario(this, "DBCUsuario", null, 1);
		SQLiteDatabase db = user.getWritableDatabase();
		if (db != null) {
			Cursor c = db.rawQuery("SELECT * FROM Usuarios", null);
			if (c.moveToFirst()) {
				id = c.getString(0);
			} else {
			}
			c.close();
		}
		db.close();
		// Log.d("user",usuario);
		registroServidor(id, regId);

	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		Log.d("GCMTest", "REGISTRATION: Desregistrado OK.");

	}

	/* #### registrar usuario #### */
	private void registroServidor(String id, String regId) {

		new asyncgcm().execute(id, regId);
	}

	/* #### mostrar notif #### */
	private void mostrarNotificacion(Context context, String msg) {
		Evento event = new Evento();
		Bundle b = new Bundle();
		JSONObject jObj = null;
		JSONArray eventos = null;
		Evento temp = new Evento();
		Log.d("NOTIFICACION", "RECIBIDA");
		/* JSON */
		final String EVENT = "eventos";
		final String eventoID = "id";
		final String eventoTitulo = "titulo";
		final String eventoLugar = "lugar";
		final String eventoFecha = "fecha";
		final String eventoStamp = "stamp";
		final String eventoTipo = "tipo";
		final String ltd = "ltd";
		final String lon = "lon";
		// Obtenemos una referencia al servicio de notificaciones
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager notManager = (NotificationManager) context
				.getSystemService(ns);

		// Configuramos la notificación
		int icono = R.drawable.ic_launcher;
		CharSequence textoEstado = "Nuevo Evento!";
		long hora = System.currentTimeMillis();

		Notification notif = new Notification(icono, textoEstado, hora);
		// AutoCancel: cuando se pulsa la notificaión ésta desaparece
		notif.flags |= Notification.FLAG_AUTO_CANCEL;
		// sonido y vibracion
		notif.defaults |= Notification.DEFAULT_SOUND;
		notif.defaults |= Notification.DEFAULT_VIBRATE;
		notif.defaults |= Notification.DEFAULT_LIGHTS;
		// Configuramos el Intent
		Context contexto = context.getApplicationContext();
		// cogemos datos del msg
		CharSequence titulo = "Nuevo Evento";

		String add = msg.toString();
		try {
			jObj = new JSONObject(add);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			eventos = jObj.getJSONArray(EVENT);
			// looping through All Contacts
			for (int i = 0; i < eventos.length(); i++) {
				JSONObject c = eventos.getJSONObject(i);
				temp.SetID(c.getString(eventoID));
				temp.SetTitulo(c.getString(eventoTitulo));
				temp.SetFecha(c.getString(eventoFecha));
				temp.SetStamp(c.getString(eventoStamp));
				temp.SetLugar(c.getString(eventoLugar));
				temp.SetTipo(c.getString(eventoTipo));
				temp.Setlon(c.getString(lon));
				temp.Setltd(c.getString(ltd));
			}
		} catch (JSONException e) {
		}
		// insertar evento en la base de datos
		// Abrir database
		DBCEvento eventDBC = new DBCEvento(this, "DBCEventos", null, 1);
		SQLiteDatabase db = eventDBC.getWritableDatabase();
		if (db != null) {

			Cursor c = db.rawQuery("SELECT * FROM Eventos WHERE eventoID='"
					+ temp.GetID() + "'", null);
			if (c.moveToFirst()) {
				// ya ingresado
				Log.d("registrado", "ya registrado");
			} else {
				// ingresar
				String sql = "INSERT INTO Eventos (eventoID,eventoTitulo,eventoLugar,eventoFecha,eventoStamp,eventoTipo,ltd,lon) VALUES "
						+ "('"
						+ temp.GetID()
						+ "','"
						+ temp.GetTitulo()
						+ "','"
						+ temp.GetLugar()
						+ "','"
						+ temp.GetFecha()
						+ "','"
						+ temp.GetStamp()
						+ "','"
						+ temp.GetTipo()
						+ "','" + temp.Getltd() + "','" + temp.Getlon() + "');";
				Log.d("registrado", sql);
				db.execSQL(sql);
			}
			c.close();
		}
		db.close();
		Log.d("evento", temp.GetID());
		b.putString("eventoID", temp.GetID());
		Intent notIntent = new Intent(contexto, EventoUno.class);
		notIntent.putExtras(b);

		PendingIntent contIntent = PendingIntent.getActivity(contexto, 0,
				notIntent, 0);
		CharSequence descripcion =msg;
		
		//CharSequence descripcion = event.GetTitulo().toString();
		CharSequence prueba=temp.GetTitulo();
		Log.d("des", prueba.toString());
		notif.setLatestEventInfo(contexto, titulo, prueba, contIntent);

		// Enviar notificación
		notManager.notify(1, notif);
	}

	/* ############### asynclogin #################### */
	class asyncgcm extends AsyncTask<String, String, String> {
		String id, gcm;

		@Override
		protected String doInBackground(String... params) {
			id = params[0];
			gcm = params[1];
			Log.d("id", id);
			// Log.d("gcm",gcm);
			gcmstatus(id, gcm);
			return "ok";
		}

	}

	/* #### GCMSTATUS #### */
	public void gcmstatus(String id, String gcm) {
		InputStream is = null;
		String result = "";
		HttpResponse r = null;
		String json2 = "";
		final String RESULT = "result";
		final String SQL = "sql";
		JSONObject json = null;

		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("id", id));
		postparameters2send.add(new BasicNameValuePair("gcmcode", gcm));
		Log.d("url", url);
		// conecta via http y envia un post.
		HttpClient client = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);

		try {
			httppost.setEntity(new UrlEncodedFormEntity(postparameters2send));

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			r = client.execute(httppost);

			// is = httpEntity.getContent();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("error", "de ejecucion");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("error", "de excepcion");
		}
		// HttpEntity entity = r.getEntity();
		try {
			HttpEntity httpEntity = r.getEntity();
			is = httpEntity.getContent();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();

			// result = sb.toString();
			/*
			 * DBCResult resultDBC = new DBCResult(this, "DBCresult", null, 1);
			 * SQLiteDatabase db =resultDBC.getWritableDatabase();
			 * db.execSQL("DELETE FROM Sql;");
			 * db.execSQL("INSERT INTO Sql (consulta) VALUES ('"+result+"');");
			 */
			// Log.d("sql",result);

			// Log.e("getpostresponse", " result= " + sb.toString());
			// Toast toast = Toast.makeText(Inicial.class, result,
			// Toast.LENGTH_LONG);
			// toast.show();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

	}
}
