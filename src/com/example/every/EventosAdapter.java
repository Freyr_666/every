package com.example.every;

import java.util.List;

import android.app.Activity;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EventosAdapter extends ArrayAdapter<Evento>{
	private Activity activity;
	private List<Evento> eventos;
	public EventosAdapter(Activity act, int resource, List<Evento> arrayList) {
		super(act, resource, arrayList);
		this.activity = act;
		this.eventos = arrayList;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		//View view = convertView;
	
		LayoutInflater inflater = activity.getLayoutInflater();
		View view= inflater.inflate(R.layout.eventos_adapter, null);

		Evento ObjEvento = eventos.get(position);
		
		TextView LblTitulo = (TextView)view.findViewById(R.id.LblTitulo);
		TextView LblFecha = (TextView)view.findViewById(R.id.LblFecha);
		TextView LblLugar = (TextView)view.findViewById(R.id.LblLugar);
		ImageView ImgFoto = (ImageView)view.findViewById(R.id.ImgFoto);
		
		
		LblTitulo.setText(ObjEvento.GetTitulo());
		LblFecha.setText(ObjEvento.GetFecha());
		LblLugar.setText(ObjEvento.GetLugar());
		
		String tipo=ObjEvento.GetTipo();
		//Log.d("tipo",tipo);
		if (tipo.equals("personal")){
			//Log.d("es personal SI",tipo);
			ImgFoto.setImageResource(R.drawable.personal);
			LblTitulo.setTextColor(0xFFFF0000);
		}else if (tipo.equals("local")){
			ImgFoto.setImageResource(R.drawable.local);
		}else if (tipo.equals("global")){
			ImgFoto.setImageResource(R.drawable.mundo);
		}
		
		return view;
	}
	
}
