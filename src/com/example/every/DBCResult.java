package com.example.every;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class DBCResult extends SQLiteOpenHelper{
	
	String sqlCreate="CREATE TABLE Sql (consulta TEXT);";

	public DBCResult(Context context, String nombre, CursorFactory factory,
			int version) {
		super(context, nombre, factory, version);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(sqlCreate);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS Sql");
		// Se crea la nueva versi�n de la tabla
		db.execSQL(sqlCreate);
		
	}
}
